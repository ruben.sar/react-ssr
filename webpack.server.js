/**
 * Created by Ruben on 06.03.2019.
 */
const webpack = require('webpack');
const path = require('path');
const nodeExternals = require('webpack-node-externals');


module.exports = {
  target: 'node',
  mode: 'production',
  entry: [
    'babel-regenerator-runtime',
    path.resolve(__dirname, 'server.prod.js')
  ],
  externals: [nodeExternals()],
  output: {
    filename: 'server.js',
    path: path.resolve(__dirname, 'build')
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true
          }
        }
      }
    ]
  }
};
