/**
 * Created by ruben on 3/4/2018.
 */

const express = require('express');
const webpack = require('webpack');
const path = require('path');
const config = require('./webpack.config.dev');
import renderer from './server/renderer';
//react
const React = require('react');
//rout
import {matchRoutes } from 'react-router-config';
import Routes from './public/components/Routes'
//redux
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './public/_reducers/index';
import thunk from 'redux-thunk';


const port = process.env.PORT || 3000;
const app = express();


const compiler = webpack(config);

app.use(express.static('public'));
app.use(express.static('assets'));

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));


app.get('*', (req, res) => {

  const store = createStore(
    rootReducer,
    {},
    applyMiddleware(thunk)
  );


  const promises = matchRoutes(Routes, req.path).map(function (item) {
    return item.route.fetchData ? item.route.fetchData(store, item.match.params) : null;
  });

  Promise.all(promises).then(()=> {
    res.send(renderer(req, store));
  });
});

app.listen(port, function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log(`running on http://localhost:${port} ...`);
  }
});
