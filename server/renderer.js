/**
 * Created by Ruben on 10/15/2018.
 */

import React from 'react';
import {renderToString} from 'react-dom/server';
import {StaticRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import App from '../public/components/App';
import {Helmet} from 'react-helmet';


export default (req, store)=> {

  const html = renderToString(
        <Provider store={store}>
            <StaticRouter location={req.path} context={{}}>
                <App />
            </StaticRouter>
        </Provider>
    );

  const helmet = Helmet.renderStatic();

    return `
 <!doctype html>
    <html lang="en">
      <head>
         ${helmet.title.toString()}
         ${helmet.meta.toString()}
         <link rel="shortcut icon" href="/img/favicon.ico">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
         <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
         <link rel="stylesheet" href="/css/custom.css" />
      </head>
      <body>
           <div id="root">${html}</div>
           <script>
              window.PRELOADED_STATE = ${JSON.stringify(store.getState()).replace(/</g, '\\u003c')}
          </script>
          <script src="/bundle.js"></script>
      </body>
   </html>
    `
}
