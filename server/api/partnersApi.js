/**
 * Created by Ruben on 12.10.2018.
 */

const express = require('express');
const api = express.Router();
const data = require('../_mockData');


const router = function () {

  api.route('/list').get(function (req, res) {

    res.status(200).send({
      partners: data.partners,
      success: true
    });

  });

  api.route('/:id').get(function (req, res) {

    const partner = data.partners.filter(partner=> {
      return partner.id == req.params.id;
    })[0];

    if (partner) {
      res.status(200).send({
        partner: partner,
        success: true
      });
    } else {
      res.status(200).send({
        partner: null,
        success: false
      });
    }

  });

  return api;
};

module.exports = router();
