/**
 * Created by ruben on 11/10/2018.
 */

import {ARTICLE}  from '../_actions/_actionTypes';
import initialState from './initialState';


export function articleReducer(state = initialState.article, action) {
  switch (action.type) {
    case ARTICLE.SINGLE_LOAD_SUCCESS:
      return action.payload;
    default :
      return state;
  }
}


export function articlesListReducer(state = initialState.articlesList, action) {
  switch (action.type) {
    case ARTICLE.LIST_LOAD_SUCCESS:
      return action.payload;
    default :
      return state;
  }
}


