/**
 * Created by ruben on 3/11/2018.
 */

import {combineReducers} from 'redux';
import {articleReducer, articlesListReducer} from './articlesReducer';


const rootReducer = combineReducers({
    article: articleReducer,
    articlesList: articlesListReducer
});

export default rootReducer;
