/**
 * Created by ruben on 3/11/2018.
 */

import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../_reducers/index';
import thunk from 'redux-thunk';

export default function configureStore(initialState) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(thunk)
  );
}
