/**
 * Created by ruben on 3/4/2018.
 */

import 'babel-polyfill';
import React from 'react';
import { hydrate } from 'react-dom';
import { BrowserRouter , Route } from 'react-router-dom'
import App from './components/App';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore.dev.js';


const preloadedState = window.PRELOADED_STATE;

delete window.PRELOADED_STATE;

const store = configureStore(preloadedState);


hydrate(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
