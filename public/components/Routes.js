/**
 * Created by ruben on 3/4/2018.
 */
import React from 'react';
import HomePage from './staticPages/HomePage';
import AboutUsPage from './staticPages/AboutUsPage';
import ContactUsPage from './staticPages/ContactUsPage';
import ArticlePage from './articles/ArticlePage';
import ArticlesListPage from './articles/ArticlesListPage';

export default [
    {
        component: HomePage,
        path: '/',
        exact: true
    },
    {
        component: AboutUsPage,
        path: '/about-us'
    },
    {
        component: ContactUsPage,
        path: '/contact-us'
    },
    {
        component: ArticlesListPage,
        path: '/articles'
    },
    {
        component: ArticlePage.component,
        path: '/article/:id',
        fetchData: ArticlePage.fetchData
    }

]
