/**
 * Created by ruben on 3/4/2018.
 */
import React from 'react';
import Header from './common/Header';
import routes from './Routes';
import {renderRoutes} from 'react-router-config';

const App = ()=> {
    return (
        <main>
            <Header />

            <div className="container">
                {renderRoutes(routes)}
            </div>
        </main>
    );
};
export default App;


