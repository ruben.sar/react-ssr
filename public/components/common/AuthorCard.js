/**
 * Created by Ruben on 19.10.2018.
 */
import React from 'react';

const AuthorCard = ({article})=> {


    return (
        <div className="author-card green-box center-block">
            <div className="as-table">
                <div className="t-cell w-20">
                    <img src={article.avatar} className="img-responsive img-round center-block"/>
                </div>
                <div className="t-cell text-center">
                    <h3>{article.author} | {article.company}</h3>
                </div>
            </div>
        </div>
    )
};
export default AuthorCard;