/**
 * Created by Ruben on 15.10.2018.
 */

import React from 'react';
import {Helmet} from 'react-helmet';

const Head = ({title, description, img})=> {
    img = img || '/img/react.png';
    return (
        <Helmet>
            <meta charSet="utf-8"/>
            <title>{`${title} | React SSR`}</title>
            <meta property="og:title" content={`${title} | React SSR`}/>
            <meta property="og:description" content={description}/>
            <meta property="og:image" content={img}/>
        </Helmet>
    );
};

export default Head;
