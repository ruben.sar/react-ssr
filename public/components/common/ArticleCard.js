/**
 * Created by Ruben on 12.10.2018.
 */

import React from 'react';
import {Link} from 'react-router-dom';

const ArticleCard = ({article})=> {


    return (
        <div className="article-card">
           <div className="text-center">
               <h3>{article.title}</h3>
           </div>

            <div className="as-table bord-b-1">
                <div className="t-cell w-20">
                    <img src={article.thumbnail} className="img-responsive center-block"/>
                </div>
                <div className="t-cell">
                    <p>{article.description}</p>
                </div>

            </div>
            <div className="as-table">
                <div className="t-cell">
                    <h4>Author | <strong>{article.author}</strong></h4>
                </div>
                <div className="t-cell">
                    <Link className="btn btn-primary" to={ `/article/${article.id}`}>Read More</Link>
                </div>

            </div>
        </div>
    )
};
export default ArticleCard;
