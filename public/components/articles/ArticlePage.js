/**
 * Created by Ruben on 12.10.2018.
 */


import React from 'react';
import {connect} from 'react-redux';
import {fetchSingleArticle} from '../../_actions/articlesActions';
import Head from '../common/Head';
import AuthorCard from '../common/AuthorCard';


class ArticlePage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    //  if (!this.props.articles.id) {
    this.props.fetchSingleArticle(this.props.match.params.id);
    //}
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.id !== this.props.match.params.id) {
      this.props.fetchSingleArticle(this.props.match.params.id);
    }
  }

  render() {
    const {article} =this.props;

    return (
      <div className="article-page">

        <Head title={`${article.title} by ${article.author}`}
              img={article.thumbnail}
              description={article.description}/>

        <div className="jumbotron">

          {!article.title && <div className="loading-box"><i className="fas fa-spinner fa-spin fa-4x"></i></div>}
          <h1 className="text-center text-blue">{article.title} </h1>
          <img src={article.thumbnail} className="img-responsive center-block" alt={article.title}/>
          <h3 className="text-blue"> {article.description}</h3>


          <p>{article.content}</p>
        </div>

        {article.title && <AuthorCard article={article}/> }
      </div>
    );
  }
}


function mapStateToProps(state, ownProps) {
  return {
    article: state.article
  };
}

const mapDispatchToProps = {
  fetchSingleArticle
};

function fetchData(store, params) {
  return store.dispatch(fetchSingleArticle(params.id));
}

export default  {
  component: connect(mapStateToProps, mapDispatchToProps)(ArticlePage),
  fetchData: fetchData
}
