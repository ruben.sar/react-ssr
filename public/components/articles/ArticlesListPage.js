/**
 * Created by ruben on 11.10.2018.
 */

import React from 'react';
import {connect} from 'react-redux';
import {fetchArticles} from '../../_actions/articlesActions';
import ArticleCard from '../common/ArticleCard';
import Head from '../common/Head';


class ArticlesListPage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
   this.props.fetchArticles();
  }

  render() {
    const {articlesList} =this.props;

    return (
      <div>
        <Head title={'Amazing Articles'} description={'React App with Client-Side Rendering'}/>

        <div className="jumbotron text-center">
          <i className="fa fa-5x text-green fa-users"></i>

          <h1>Amazing Articles</h1>
        </div>
        <div className="row">
          {articlesList.map(article=> {
            return <div className="col-sm-6" key={article.id}>
              <ArticleCard article={article}/>
            </div>
          }) }
          {articlesList.length===0 && <div className="loading-box"><i className="fas fa-spinner fa-spin fa-4x"></i></div>}
        </div>

      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    articlesList: state.articlesList
  };
}

const mapDispatchToProps = {
  fetchArticles
};


export default  connect(mapStateToProps, mapDispatchToProps)(ArticlesListPage);



