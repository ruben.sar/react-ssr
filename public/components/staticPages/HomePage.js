/**
 * Created by Ruben on 3/4/2018.
 */

import React from 'react';
import Head from '../common/Head';

const HomePage = ()=> {

  return (
    <div>
      <Head title={'Home Page'} description={'React App with Server-Side Rendering'}/>

      <div className="jumbotron text-center">
        <i className="fab fa-5x text-blue fa-react"></i>

        <h1>Home Page</h1>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-sm-6">
            <div className="text-center">
              <h3><i className="fas fa-users fa-3x"></i></h3>

              <h3>Our Customers</h3>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut enim ad minim veniam, quis nostrud
              exercitation ullamco laboris...</p>

          </div>
          <div className="col-sm-6">
            <div className="text-center">
              <h3><i className="fas fa-3x fa-handshake"></i></h3>

              <h3>Our Partners</h3>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut enim ad minim veniam, quis nostrud
              exercitation ullamco laboris...</p>

          </div>
        </div>
      </div>
    </div>
  );
};
export default HomePage;

