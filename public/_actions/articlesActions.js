/**
 * Created by ruben on 3/12/2018.
 */
import {ARTICLE}  from './_actionTypes';
import axios from 'axios';

export const fetchArticles = (id)=> async dispatch => {
  const res = await axios.get('https://jsconf-api.herokuapp.com/articles/list');
  return dispatch({type: ARTICLE.LIST_LOAD_SUCCESS, payload: res.data});
};


export const fetchSingleArticle = (id)=> async dispatch => {
    const res = await axios.get(`https://jsconf-api.herokuapp.com/articles/${id}`);
    return dispatch({type: ARTICLE.SINGLE_LOAD_SUCCESS, payload: res.data});
};
