/**
 * Created by Ruben on 04.03.2019.
 */
/**
 * Created by ruben on 3/4/2018.
 */

const express = require('express');
import path  from 'path';
import renderer from './server/renderer';
import React from 'react';
import {matchRoutes } from 'react-router-config';
import Routes from './public/components/Routes'
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './public/_reducers/index';
import thunk from 'redux-thunk';
import compression from 'compression';

const port = process.env.PORT || 3000;
const app = express();

app.use(compression());
app.use(express.static('public'));
app.use(express.static('assets'));
app.use(express.static('dist'));

app.use('/robots.txt', function (req, res, next) {
  res.type('text/plain');
  res.send("User-agent: *\nDisallow:");
});

app.get('*', (req, res) => {

    const store = createStore(
        rootReducer,
        {},
        applyMiddleware(thunk)
    );

    const promises = matchRoutes(Routes, req.path).map(function (item) {
        return item.route.fetchData ? item.route.fetchData(store, item.match.params) : null;
    });

    Promise.all(promises).then(()=> {
        res.send(renderer(req, store));
    });
});

app.listen(port, function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log(`running on http://localhost:${port} ...`);
    }
});
